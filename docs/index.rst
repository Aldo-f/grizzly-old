.. De Grizzly documentation master file, created by
   sphinx-quickstart on Wed Jul 10 08:14:33 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentatie voor De Grizzly
============================

.. toctree::
   :maxdepth: 2
   :caption: Inhoud:

   docs/dieren
   docs/functionaliteit
  

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Dit Project
===========

| Een Drupal 8 website als alternatief voor de vorige WordPress website.
| Dit voor een betere integratie te verkrijgen met een CRM systeem zoals CiviCRM.


Functies
--------

- JSON-import van FOS 
- Totemzoeker op basis van kenmerken
- Winkeltje
- Documenten beschikbaar stellen
- Account beheren
- ..._

.. _...: /functionaliteit.rst

Installatie
------------

| De repository maakt gebruik van Docker.
| Een ``docker-compose up -d`` zou deze local envirioment moeten opstarten.
| Nadien m.b.h.v. volgende commando's kan u de ``dump.sql`` importeren.

- ``make shell``
- ``drush sql-cli < ./mariadb-init/dump.sql`` 


Bijdragen?
----------

- Problemen melden: `gitlab.com/Aldo-f/grizzly/issues`_
- Broncode: `gitlab.com/Aldo-f/grizzly`_

.. _gitlab.com/Aldo-f/grizzly/issues: https://gitlab.com/Aldo-f/grizzly/issues
.. _gitlab.com/Aldo-f/grizzly: https://gitlab.com/Aldo-f/grizzly/


Hulp?
-------

Je kan ons vinden op `telegram`_.

.. _telegram: https://t.me/DeGrizzly

