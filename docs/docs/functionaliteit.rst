Algemene functionaliteit
========================
.. toctree::
   :maxdepth: 2
   :caption: Inhoud:

   functionaliteit/takken



- [x] JSON-import van FOS 
- [X] Totemzoeker op basis van kenmerken
- [ ] Winkeltje
- [ ] Documenten beschikbaar stellen
- [ ] Account beheren

<ul>
<li><input disabled="" type="checkbox"> foo</li>
<li><input checked="" disabled="" type="checkbox"> bar</li>
</ul>

| - [x] foo
|  - [ ] bar
|  - [x] baz
| - [ ] bim